Package Registry 
==========

The system is empowered by a Blockchain. Our custom implementation is based on openethereum.

Introduction
------------------
The OurVillage project has implemented a Python Package Index (PyPI) to manage application dependencies independently, eliminating the dependency on the Grassroot system.
The registry is accessible via the domain https://pip.bafoka.network.

Objective
-----------------------------
The main objective of this deployment is to ensure efficient management of OurVillage application dependencies, providing full control over the versions of the used packages.

Accessing the Registry
-----------------------------
The OurVillage registry is accessible at the following address: https://pip.bafoka.network

Configuration
-----------------------------
To use the OurVillage registry in your Python project, add the following configuration to your `requirements.txt` or `pyproject.toml` file:

```bash
--index-url https://pip.bafoka.network/simple/
```

.. image:: ../img/pip.bafoka.network.png
   :width: 600