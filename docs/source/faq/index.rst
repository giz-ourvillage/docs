FAQ
===

What is the code to have access to the USSD service ?
-----------------------------------------------------

In order to access the USSD service in Cameroon, you need to dial #065# and follow the instructions.


how the system is structured ?
-----------------------------------------------------

Before we can start, it is imperative to know the system in its entirety by reading all the documentation and operation. 

The system is a microservices architecture with about 15 services deployed on a kubernetes cluster with a Node on digitalocean and the servers for the database and the cache which is Redis. 

Here is the diagram of the system

.. image:: ../img/architecture.png
   :width: 600

.. image:: ../img/dash.png
   :width: 600


Where is the node located?
-----------------------------------------------------
The system that is currently deployed has a single node of the digitalocean at the ip address: http://164.90.208.168:8545

CHAIN_SPEC=evm:aura:23723:bafoka


How are the logs managed?
-----------------------------------------------------
The system being deployed on kubernetes the logs are managed by Grafana which is a powerful log management tool, all the deployments send the logs back to this application and the dashboard allows to see the different logs and also the health of the cluster. 


.. image:: ../img/1.png
   :width: 600

.. image:: ../img/2.png
   :width: 600

.. image:: ../img/5.png
   :width: 600

.. image:: ../img/6.png
   :width: 600

.. image:: ../img/7.png
   :width: 600
Where to see the metrics of the system operation?
-----------------------------------------------------
To see the metrics of a particular service you have to go to grafana and then node and then select the service you want and then choose logs, a page will open and you can navigate in the logs and also make filtering requests. 

.. image:: ../img/3.png
   :width: 600

.. image:: ../img/4.png
   :width: 600

How do I know that the USSD is functional?
-----------------------------------------------------

The first thing to do when we realize that the ussd is not working is to know if our Nexah partner is able to send us the request, so we have to open the logs on grafana and see if we receive an incoming request. 

If it is not the case, the problem is probably with the partner (it may be in maintenance)


If we receive the request, in this case there may be a bug (please try to understand by consulting the logs) 

If the system is very saturated, the delay in the server's response can cause a timeout in the user's session. In this case, it is necessary to optimize and fix what slows down the system.

What are the different problems that the USSD service may encounter?
-----------------------------------------------------
The Nexah partner may not return the requests correctly to our server.

System saturation that causes user session timeout.

Lack of cached variable necessary for the system operation (for example active village, active account, active language, active voucher)



What are the problems that can impact the execution of a transaction in the blockchain?
-----------------------------------------------------
When you execute a transaction from the USSD or the mobile application and you realize that it does not work, there are two main problems:

The account that performed the transaction does not have enough gas fee and the gifter's account is empty 
Solution: You have to supply the gifter with gas because it is the one that allows you to make the gift to the account that makes transactions.

Or the nonce of the user's account is incorrect compared to what the blockchain expects.
Solution: In this case we need to query the correct nonce of the account by making a request to the node and then put the correct nonce in the database for the account. 


How is the link made between the USSD and the user's phone? 
-----------------------------------------------------


Where can we see the list of services and how they work?
-----------------------------------------------------
The services being deployed on a Kubernetes cluster you will have to (if you have access) open the cluster interface in the deployment session to see the different services.

The pods session contains the different docker containers of the services that are running, it is also possible to see the logs in each pod but they are not structured and the retention time is very small. 

.. image:: ../img/logs.png
   :width: 600

.. image:: ../img/pods.png
   :width: 600

# How to run a validator node?
------------------------------

In order to run a validator node, checkout the instructions available at https://gitlab.com/giz-ourvillage/bafoka-chain.


# Why are transactions slow to execute and notifications not arriving?
------------------------------
This problem is due to congestion in the Redis task chain, in fact in the system many events are created and executed in different order.

After some time, even a few months, it would be wise to empty the Redis cache and restart the various services (preferably during the time when the system is not in use, between 3 and 4 a.m.).

The order in which services are restarted should follow the order in the Docker-compose. (restart only services using Redis).

After restart, the transactions that were blocked will run until the system reaches normal operating state.