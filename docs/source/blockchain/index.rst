Blockchain 
==========

The system is empowered by a Blockchain. Our custom implementation is based on openethereum.

About OpenEthereum
------------------

Fast and feature-rich Ethereum client.

Built for mission-critical use: Miners, service providers, and exchanges need fast synchronisation and maximum uptime. 
OpenEthereum provides the core infrastructure essential for speedy and reliable services. Clean, modular codebase for easy customisation Advanced CLI-based client
Minimal memory and storage footprint Synchronise in hours, not days with Warp Sync Modular for light integration into your service or product

From a technical perspective, OpenEthereum's goal is to be the fastest, lightest, and most secure Ethereum client. We are developing OpenEthereum using the cutting-edge Rust programming language. OpenEthereum is licensed under the GPLv3 and can be used for all your Ethereum needs.
By default, OpenEthereum runs a JSON-RPC HTTP server on port :8545 and a Web-Sockets server on port :8546. This is fully configurable and supports a number of APIs.

Bafoka Netowrk implementation
-----------------------------

Our implementation uses PoA for creating blocks and securing the network.

The network ID is: 23723. The full chain specification is: evm:aura:23723:bafoka.

For instructions on how to run and participate in the network check out https://gitlab.com/giz-ourvillage/bafoka-chain.


