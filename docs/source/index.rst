Welcome to Bafoka documentation !
===================================

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: 🚀 Blockchain

   /blockchain/index


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: 💡 Software

   /software/index
   /software/principles

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: 📦 Package Registry

   /registry/index

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: 🪄 FAQ

   /faq/index

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: 📚 Glossary

   /glossary/index


.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: 📚 Reference

   /reference/index
   