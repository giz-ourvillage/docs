CIC Software
============

Technology Setup
----------------

.. image:: ../img/architecture.png
   :width: 600

Generally the technical setup is done by a local Platform Service
Provider like Grassroots Economics. The more local the better.

1. **Open Source (CopyLeft) Software**: Ensure that you trust the
   software being used and that even if it is open source now, that
   improvements and upgrades will remain open source. Check out our
   `stack  </software/>`_.
2. **Distributed Ledger**: We highly recommend developing your own
   ledger system where members of the community hold nodes that
   decentralize and secure the ledger.
3. **Interfaces / Wallets**: We’ve built custodial systems that enable
   users to assign guardians that can help them reset lost passwords.
4. **Data Sharing**: Given the consent of the community, anonymous
   transaction data can be recorded and displayed

CIC Stack Summary
-----------------

While work toward *non-custodial* and web interfaces are underway, the
CIC Stack is currently a *custodial* wallet and blockchain bidirectional
interface engine for community inclusion currencies

-  Fully Open source GPL 3.0 License
-  Automates the full lifecycle of blockchain transactions
-  Chain agnostic by design
-  Introduces a new chain library toolset written from scratch
-  Modular design with fully standalone but recombinable components
-  Includes a broad collection CLI tools for most common chain and
   engine operations
-  Empowers implementers to choose their level of abstraction
-  Supports both local, containerized and decentralized environments
-  Support last mile front-end apis and integrations

System parts
------------

-  **Base components**, containing all necessary provisions for services
   and tooling. (`Chaintools  <https://git.grassecon.net/chaintool>`_)
-  **Generic services components**, libraries, daemons and services
   making up the custodial engine.
   (`cicnet  <https://git.grassecon.net/chaintool>`_)
-  **Deployment components**, which facilitates deployment of the
   custodial engine, as well as seeding data for development,
   demonstration and migration.
   (`GrassrootsEconomics  <https://git.grassecon.net/grassrootseconomics>`_)
-  `Test Coverage  <https://coverage-reports.grassecon.net/>`_

Base components
---------------

-  **Queue handling**
   (`chainqueue  <https://git.grassecon.net/chaintool/chainqueue>`_):
   Makes sure that a transaction sent on behalf of a user is sent,
   resubmitted if stalled, and whose execution is verified by quering
   the network state.
-  **Chain syncing**,
   (`chainsyncer  <https://git.grassecon.net/chaintool/chainsyncer>`_):
   Retrieves all state changes from the network and executes an arbitary
   number of code fragments per transaction.
-  **RPC (Remote Procedure Call) interface**, which divides in two
   parts:

   1. **Generic interface**
      (`chainlib  <https://git.grassecon.net/chaintool/chainlib>`_):
      Concepts common to all chain-like RPCs, and provides a thread-safe
      interaction framework.
   2. **Ethereum interface**
      (`chainlib-eth  <https://git.grassecon.net/chaintool/chainlib-eth>`_):
      An extension of the former for the Ethereum/EVM network.

-  **Chain tooling**
   (`chainlib  <https://git.grassecon.net/chaintool/chainlib>`_,
   `chainlib-eth  <https://gitlab.com/chaintool/chainlib-eth>`_):
   Granular access to all conceptual layers of chain interaction,
   including binary serializations and application interfaces, along
   with CLI (Command Line Interface) tooling framework.
-  **Signer**,
   (`crypto-dev-signer  <https://git.grassecon.net/chaintool/crypto-dev-signer>`_)
   Low-security keystore and signer which is easily usable in both
   production (provided that no external access is possible) and in any
   development environment.
-  **Configuration**
   (`confini  <https://gitlab.com/nolash/python-confini>`_):
   Incrementally merging configuration definition from multiple modules,
   and easily overriding them with command line flags and environment
   variables.

Generic services components
~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  All smart contract wrappers in the
   `cicnet  <https://git.grassecon.net/cicnet>`_ repository.
-  **CIC Contract registry**
   (`cic-eth-registry  <https://gitlab.com/grassrootseconomics/cic-eth-registry>`_):
   Defines token and resource pointer lookup and authentication
   resources.
-  Daemons and microservices in the **apps** subdirectories in the
   `cic-internal-integration  <https://gitlab.com/grassrootseconomics/cic-internal-integration>`_
   monorepo, specifically:

   -  **cic-eth**: Massively parallel and fault-tolerant implementation
      of the custodial signer/queuer/syncer engine, accepting tasks from
      end-users via middleware.
   -  **cic-cache**: Cache syncer and database fetching and storing
      details on transactions of interest.
   -  **cic-ussd**: State machine, microservices and gateway for
      end-users using the USSD (Unstructured Supplementary Service Data)
      interface via telcos.
   -  **cic-notify**: Pluggable notification engine, initially used only
      for SMS notifications to end-users.

Deployment components
~~~~~~~~~~~~~~~~~~~~~

-  Data seeding and/or migrations for new development deployments.
   Located in the **apps/data-seeding** subdirectory of
   `cic-internal-integration  <https://gitlab.com/grassrootseconomics/cic-internal-integration>`_.
-  Deployment and initialization of network resources (smart contracts
   etc) and initialization of the custodial engine. Located in the
   **apps/contract-migrations** subdirectory of
   `cic-internal-integration  <https://gitlab.com/grassrootseconomics/cic-internal-integration>`_.

Components by category
----------------------


`Link text  <link URL>`

(in no particular order)

+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Area                          | Category      | Components                                                                                                                                                                                                                                                                                            | Tech                 | Relative Complexity | Maturity |
+===============================+===============+=======================================================================================================================================================================================================================================================================================================+======================+=====================+==========+
| Chain Queue/sync Base         | Any           | `chainqueue <https://git.grassecon.net/chaintool/chainqueue>`_, `chainsyncer <https://git.grassecon.net/chaintool/chainsyncer>`_                                                                                                                                                                      | Py                   | Mid                 | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Chain Queue/sync Engine       | Custodial     | `cic-Eth <https://gitlab.com/grassrootseconomics/cic-Internal-Integration/-/tree/master/apps/cic-Eth>`_, `cic-Eth-Registry <https://gitlab.com/grassrootseconomics/cic-Eth-Registry>`_, `cic-Signer <https://gitlab.com/grassrootseconomics/cic-Internal-Integration/-/tree/master/apps/cic-Signer>`_ | Py, Celery           | Hi                  | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Notifications                 | Custodial     | `cic-Notify <https://gitlab.com/grassrootseconomics/cic-Internal-Integration/-/tree/master/apps/cic-Notify>`_                                                                                                                                                                                         | Py, Celery           | Lo                  | Mid      |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Ussd Wallet                   | Custodial     | `cic-Ussd <https://gitlab.com/grassrootseconomics/cic-Internal-Integration/-/tree/master/apps/cic-Ussd>`_                                                                                                                                                                                             | Py, Celery           | Hi                  | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Wallet Interface Translations | Any           | `cic-Translations <https://gitlab.com/grassrootseconomics/cic-Internal-Integration/-/tree/master/apps/cic-Eth>`_                                                                                                                                                                                      | Yaml?                | Lo                  | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| HTTP Authentications          | Custodial     | `usumbufu <https://git.defalsify.org/python-Usumbufu/>`_, `cic-Auth-Helper <https://gitlab.com/grassrootseconomics/cic-Auth-Helper>`_                                                                                                                                                                 | Py                   | Hi                  | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Python Chain Libaries         | Any           | `chainlib-Eth <https://git.grassecon.net/chaintool/chainlib-Eth>`_, `chainlib <https://git.grassecon.net/chaintool/chainlib-Eth>`_                                                                                                                                                                    | Py                   | Mid                 | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Python Signer Libraries       | Any           | `funga <https://git.grassecon.net/chaintool/funga>`_, `funga-Eth <https://gitlab.com/grassrootseconomics/cic-Eth-Registry>`_                                                                                                                                                                          | Py                   | Mid                 | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Local Queue And Sync          | Non-Custodial | `chaind <https://git.grassecon.net/chaintool/chaind>`_, `chaind-Eth <https://git.grassecon.net/chaintool/chaind-Eth>`_, `cic-Batch <https://git.grassecon.net/grassrootseconomics/cic-Batch>`_                                                                                                        | Py                   | Mid                 | ?        |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Contract Interfaces           | Any           | `cic-Contracts <https://git.grassecon.net/cicnet/cic-Contracts>`_, `eth-Erc20 <https://gitlab.com/cicnet/eth-Erc20>`_, `eth-Owned <https://gitlab.com/cicnet/eth-Owned>`_, `erc20-Faucet <https://gitlab.com/cicnet/erc20-Faucet>`_, `eth-Interface <https://gitlab.com/cicnet/eth-Interface>`_       | Solidity, Py, Bash   | Mid                 | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Ge Token Contract             | Any           | `erc20-Demurrage-Token <https://gitlab.com/cicnet/erc20-Demurrage-Token>`_, `sarafu-Faucet <https://gitlab.com/grassrootseconomics/sarafu-Faucet>`_                                                                                                                                                   | Solidity, Py         | Hi                  | Hi/lo    |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Verificable Claims Base       | Any           | `okota <https://git.grassecon.net/cicnet/okota>`_                                                                                                                                                                                                                                                     | Solidity, Py         | Mid                 | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Cic Analytics And Aggregator  | Custodial     | `cic-Data-Golang <https://gitlab.com/grassrootseconomics/cic-Data-Golang>`_                                                                                                                                                                                                                           | Golang               | Mid                 | Mid      |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Cic Analytics Views           | Custodial     | `cic-Data-Golang <https://gitlab.com/grassrootseconomics/cic-Data-Golang>`_                                                                                                                                                                                                                           | Grafana, Golang?     | Mid                 | ?        |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Web2 Metadata Store           | Custodial     | `cic-Meta <https://gitlab.com/grassrootseconomics/cic-Internal-Integration/-/tree/master/apps/cic-Meta>`_                                                                                                                                                                                             | Typescript           | Mid                 | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Metadata Protocol             | Any           | `crdt-Meta <https://gitlab.com/cicnet/crdt-Meta>`_                                                                                                                                                                                                                                                    | Typescript           | Mid                 | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Metadata Schemas              | Any           | `cic-Types <https://gitlab.com/grassrootseconomics/cic-Types>`_                                                                                                                                                                                                                                       | Py                   | Mid                 | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Contributor Badge Nft         | Non-Custodial | `rose Bootcamp Project <https://rapid-Silence-3500.on.fleek.co/>`_, `nft-Badgetoken <https://git.defalsify.org/evm-Badgetoken/>`_                                                                                                                                                                     | Louis                | Mid                 | Lo       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Web3 Transition Adapters      | Any           | `fadafada <https://git.defalsify.org/fadafada/>`_, `fadafada-Curl <https://git.defalsify.org/fadafada-Curl/>`_                                                                                                                                                                                        | Rust                 | Mid                 | Mid      |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Cli Cic Creation              | Any           | `cic-Cli <https://git.grassecon.net/cicnet/cic-Cli>`_                                                                                                                                                                                                                                                 | Py                   | Hi                  | Mid      |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Cli Metadata Viewer           | Custodial     | `clicada, Cic-Staff-Installer <https://git.grassecon.net/grassrootseconomics/cic-Staff-Client>`_                                                                                                                                                                                                      | Py                   | Lo                  | Lo       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Ge Blog                       | Website       | `pelican Website <https://git.grassecon.net/grassrootseconomics/pelican-Website-Ge>`_                                                                                                                                                                                                                 | Py, Pelican          | Lo                  | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Ge Docs                       | Website       | `docs Webstie <https://gitlab.com/grassrootseconomics/grassrootseconomics.gitlab.io>`_                                                                                                                                                                                                                | Py, (platform)?      | Mid?                | Mid      |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Local Dev Bootstrap           | Devops        | `cic-Stack <https://gitlab.com/grassrootseconomics/cic-Internal-Integration>`_                                                                                                                                                                                                                        | Bash, Docker-Compose | Hi                  | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Dev Data Seeding / Migration  | Any           | `cic-Stack <https://gitlab.com/grassrootseconomics/cic-Internal-Integration>`_                                                                                                                                                                                                                        | Py, Bash, Typecsript | Hi                  | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Telegram Wallet               | Custodial     | ?                                                                                                                                                                                                                                                                                                     | ?                    | ?                   | No       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Chain Queue/sync Interface    | Custodial     | `cic-Eth-Server <https://gitlab.com/grassrootseconomics/cic-Internal-Integration/-/merge_requests/312>`_                                                                                                                                                                                              | Py                   | ?                   | ?        |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Web2 Wallet                   | Custodial     | `social Recovery Protocol <https://gitlab.com/grassrootseconomics/cic-Docs/-/blob/lash/social-Recovery/spec/025_social_recovery.md>`_                                                                                                                                                                 | ?                    | ?                   | No       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| P2P Services Discovery        | Any           | `cic-P2p <https://gitlab.com/grassrootseconomics/cic-P2p>`_, `village Message Protocol <https://gitlab.com/cicnet/vmp>`_                                                                                                                                                                              | ?                    | ?                   | ?        |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Gas Helpers                   | Any           | `cic-Gas-Proxy <https://gitlab.com/grassrootseconomics/eth-Gas-Proxy/-/blob/master/gas_proxy/proxy.py>`_, `ethd-Gas-Sum <https://git.grassecon.net/chaintool/ethd-Gas-Sum>`_                                                                                                                          | Py                   | Lo                  | Lo       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Swarm Provisions              | Non-Custodial | `libswarm-Ng <https://git.defalsify.org/libswarm-Ng/>`_, `pylibswarm <https://git.defalsify.org/pylibswarm/>`_                                                                                                                                                                                        | C, Py                | Mid                 | Hi       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+
| Multisig                      | Non-Custodial | `erc20-Transfer-Authorization <https://gitlab.com/cicnet/erc20-Transfer-Authorization>`_                                                                                                                                                                                                              | Solidity, Py         | Mid                 | Lo       |
+-------------------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+---------------------+----------+

(“Relative complexity” means complexity in relation to the function of
the component.)